// Main content script

// const manifest = {
//   "name": "DEV Community",
//   "short_name": "dev.to",
//   "description": "A constructive and inclusive social network for software developers. With you every step of your journey.",
//   "start_url": "/",
//   "display": "standalone",
//   "background_color": "#000000",
//   "theme_color": "#000000",
//   "homepage_url": "https://dev.to",
//   "icons": [{
//     "src": "https://res.cloudinary.com/practicaldev/image/fetch/s--t7tVouP9--/c_limit,f_png,fl_progressive,q_80,w_192/https://practicaldev-herokuapp-com.freetls.fastly.net/assets/devlogo-pwa-512.png",
//     "sizes": "192x192",
//     "type": "image/png",
//     "purpose": "any maskable"
//   }, {
//     "src": "https://res.cloudinary.com/practicaldev/image/fetch/s--lrmEcD2H--/c_limit,f_png,fl_progressive,q_80,w_128/https://practicaldev-herokuapp-com.freetls.fastly.net/assets/devlogo-pwa-512.png",
//     "sizes": "128x128",
//     "type": "image/png",
//     "purpose": "any maskable"
//   }, {
//     "src": "https://res.cloudinary.com/practicaldev/image/fetch/s--PFPhV65b--/c_limit,f_png,fl_progressive,q_80,w_512/https://practicaldev-herokuapp-com.freetls.fastly.net/assets/devlogo-pwa-512.png",
//     "sizes": "512x512",
//     "type": "image/png",
//     "purpose": "any maskable"
//   }]
// }

const manifest = {
  "name": "IMDb",
  "short_name": "IMDb",
  "description": "The internet movie database",
  "start_url": "/",
  "display": "standalone",
  "background_color": "#000000",
  "theme_color": "#000000",
  "homepage_url": "https://m.imdb.com",
  "icons": [{
    "src": "https://cdn4.iconfinder.com/data/icons/logos-and-brands/512/171_Imdb_logo_logos-512.png",
    "sizes": "512x512",
    "type": "image/png",
    "purpose": "any maskable"
  }]
}

// const manifest = {
//   "name": "GitHub Gists",
//   "short_name": "GitHub Gists",
//   "description": "GitHub Gists - My Gists",
//   "display": "standalone",
//   "background_color": "#000000",
//   "theme_color": "#000000",
//   "homepage_url": "https://gist.github.com/jonrandy",
//   "icons": [{
//     "src": "https://cdn3.iconfinder.com/data/icons/inficons/512/github.png",
//     "sizes": "512x512",
//     "type": "image/png",
//     "purpose": "any maskable"
//   }]
// }



function go() {
	const b64Manifest = btoa(JSON.stringify(manifest))
  const link = document.createElement("link");
  link.setAttribute("rel", "manifest")
  link.setAttribute('href', `data:application/json;base64,${b64Manifest}`)
  document.head.appendChild(link)
  // document.head.insertAdjacentElement("afterbegin", link)
 }

go(manifest)